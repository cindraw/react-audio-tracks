# React Audio Tracks 

by Cedric Indra Widmer, cedricindra@gmail.com

## Install

```
git clone https://cindraw@bitbucket.org/cindraw/react-audio-tracks.git
cd react-audio-tracks
npm install
npm start 
```
### **Open with CHROME or FIREFOX, no Safari !!!**

## Developer Notes 

The Web Audio API is a far richer environment to treat audio than HTML audio. I

Imported files are uploaded and stored on a server.

This is built on a custom boilerplate.

___
## Usage
- Click upload to load a track (only one per upload).
- Click upload again to load a second track.
- Click play and marvel at the visualizations.
- Use volume sliders and play/pause buttons for controls.

All uploaded files are saved in ./server/uploads. Make sure you dump the contents of this folder as uploaded files are accumulated.
Reload the page to reset the app and try again.

___
## Development

### Goals
- Create a server so as to save uploaded audio files.
- Create an interface with react to upload and playback files.
- Use the Web Audio API to setup audio busing to use effects :
	- connect and control volume with the gain node. 
	- access the audio analyser to create a spectrometer.
- univeral Play/Pause button
- use react for a framework using hooks and context (as opposed to redux which is falling out of favor).

### Steps
- Setup a boilerplate with webpack and babel.
- Setup expressjs backend.
- Setup middlewares to process file uploads.
- Setup reactjs as the frontend.
- Setup store, context and actions to handle state between hooks.
- Create a simple interface to upload files to the server then immediately download the audio files to ready for playback on individual tracks with dedicated volume controls.
- Setup audiocontext via the Web Audio API to handle all audio playback and visualizations.
- Setup visualizations.

### Issues
- There's a reason why the usage is restricted to the steps detailed in the User Manual of this readme such as: one cannot load a track once play is hit. This is because the web api needs to be defined to allow loose interpretation of user inputs. However, the time was rather allocated to explore audio analysis and visualizations.
- Safari is not very happy with createMediaElementSource so please restrict usage of this app in more developer friendly browsers (Chrome, Firefox) until this issue is fully addressed.
___
## Future Development

### Front End
- Unit tests.
- Better design and responsiveness.
- Migrate everything to the Canvas API for better viz performance.
- Keyboard controls.
- Address Safari bug.
- Code for looser interpretation of user inputs.
- Advanced functionality : 
	- solo/mute individual channels
	- panning
	- scrubbing
	- looping
- Waveform visualization over whole track...

**Basically, be more of a DAW on the front end!**


### Environment
- Setup AWS S3 bucket for cloudless hosting for development, testing and production.
- Integrate node script in package to handle build and deploy.
- Create development environment.
- Create testing environment.
- Create production environment.