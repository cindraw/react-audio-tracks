/*
A simple local server with middlewares to handle receiving and storing files to the uploads folder
*/

require('dotenv').config()
const { PORT, DEVSERVER } = process.env
const express = require('express')
const bodyParser = require('body-parser')
const multer = require('multer')
const uuidv4 = require('uuid/v4')
const path = require('path')
const cors = require('cors')
const uploads_folder = 'server/uploads'


// configure storage
const storage = multer.diskStorage({
	destination: (req, file, cb) => {
		// Files will be saved in the 'uploads' directory. Make sure this directory already exists!
		cb(null, uploads_folder)
	},
	filename: (req, file, cb) => {
		/* generate unique filename with uuidv4, return object with info */
		const newFilename = `${uuidv4()}${path.extname(file.originalname)}`
		cb(null, newFilename)
	},
})
// create the multer instance that will be used to upload/save the file
const upload = multer({ storage })

const app = express()

app.use(cors())
app.use('/uploads', express.static(uploads_folder))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

/* upload file, run through multer middleware, send file reference object back */
app.post('/', upload.single('audioFile'), (req, res) => {
	console.log('uploaded: ', req.file.originalname)
	res.send(req.file)
	res.end()
})

app.get('/', (req, res) => {
	res.send(`
  	<a href="#" onClick="window.open('http://localhost:${DEVSERVER}', '_blank')">
  	<div style="
	  	display:block;
		position:fixed;
		top:50%;
		left:50%;
		background:red;
		color:black;
		transform:translate(-50%,-50%);
		padding:5px;
		border-radius:5px">
			Server listening on port: ${PORT}
	</div>
  </a>`)
	res.end()
})

app.listen(PORT, () => {
	console.log(`Server listening on port ${PORT}`) }
)