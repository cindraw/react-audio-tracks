/*
fetches audio and returns an array buffer
 */

export default async function getFile(filepath) {
	const response = await fetch(filepath)
	const arrayBuffer = await response.arrayBuffer()
	console.log(arrayBuffer)
	return arrayBuffer
  }