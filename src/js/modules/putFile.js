/*
puts form data = audio into the server
returns data = res.json()
 */

export default async function putFile(server, file) {

	let formData = new FormData()
	formData.append('audioFile', file)

	const options = {
		method: 'POST',
		body: formData
	}

	try {
		const res = await fetch(server, options)
		const data = await res.json()
		return data
	}

	catch (err) {
		return err
	}
}
