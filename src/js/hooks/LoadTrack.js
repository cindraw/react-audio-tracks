/*
Calssic HTML load file form with css removing all the common eleents.
Accepts only audio files by checking mime types in the actions.
Logs file to state.audio_files_for_upload
*/


import React, { useContext, useState, useEffect } from 'react'
import { StoreContext } from '../context/store'
import { types } from '../context/reducers'

const LoadTrack = () => {
	const { state, dispatch, actions } = useContext(StoreContext)

	const onChange = e => {
		e.preventDefault()
		const file = e.target.files[0]
		const isAudio = file ? actions.checkMimeTypeIsAudio(file) : false
		if (isAudio) {
			actions.logAudioFileToUploadQueu(file)
			actions.logMessage('Audio file ready for upload')
		} else {
			actions.logMessage('This is not an audio file')
		}
	}

	return (
		<>
			<label className="custom-button">
				<input
					type="file"
					name="selectedFile"
					multiple={false}
					onChange={onChange}
				/>
				Upload File
			</label>
		</>
	)

}

export default LoadTrack