/*
Tracks is a display and volume slider for information given from parent element
voume value is sent to state.track_volumes to be used in the parent hook: Workstation.js
*/


import React, { useContext, useState, useEffect } from 'react'
import { StoreContext } from '../context/store'
import { types } from '../context/reducers'
import Spectrometer from './Spectrometer'


const Track = ({ data, index }) => {

	const { state, dispatch, actions } = useContext(StoreContext)
	const [volume, setVolume] = useState(0.75)
	const originalname = data.originalname.split('.')[0] //remove filename extension

	const handleTrackVolume = e => {
		const value = Number(e.target.value)
		setVolume(value)
		actions.setTrackVolumes({ id: index, num: volume })
		console.log(originalname, index)
	}

	// on load set default volume
	useEffect(() => { actions.setTrackVolumes({ id: index, num: volume }) }, [index])

	const track = (
		<div className="track" >
			<div className="track-menu-left">{Math.trunc(volume * 24)}</div>
			<div className="track-menu-right">{originalname}</div>
			<div className="slider-container">
				<section>
					<input className="slider"
						onChange={handleTrackVolume}
						type="range"
						id="volume"
						min="0"
						max="1"
						defaultValue={volume}
						step="0.01"
						data-action="volume" />
				</section>
			</div>
			<div className="spectrometer-grid-container"><Spectrometer index={index} /></div>
		</div>
	)

	return track
}

export default Track