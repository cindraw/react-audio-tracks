import React, { useContext, useState, useEffect } from 'react'
import { StoreContext } from '../context/store'
import { types } from '../context/reducers'

const Spectrometer = ({ index }) => {
	const { state, dispatch, actions } = useContext(StoreContext)
	const [data, setData] = useState(undefined)
	const [bars, setBars] = useState(undefined)
	const [volume, setVolume] = useState(undefined)
	const [container, setContainer] = useState(undefined)

	useEffect(() => {
		const container = document.querySelector('.spectrometer-container').offsetWidth
		setContainer(container)
	})

	useEffect(() => {
		// convert Uint8bit to Array
		if (state.frequency_data[index]) setData(Array.from(state.frequency_data[index]))
		if (state.new_track_volumes[index]) setVolume(state.new_track_volumes[index])
	}, [state.frequency_data, state.new_track_volumes])

	useEffect(() => {
		if (!data) { return }
		// create individual bars for spectrometer
		// truncate the higher frequencies as there is no data there and will look better for visualisations
		//  680 is roughly 66% of 1024
		const length = 680
		const new_data = data.slice(0, length)
		//

		const spectrometer = new_data.map((q, index) => {
			// transform individual bars and calculate volume so spectrometer reflects volume input
			const transform = (q / 255 * volume).toFixed(2)
			const responsive_width = (container / length).toFixed(2)
			const style = {
				width: responsive_width + 'px',
				transform: `scaleY(${transform})`,
			}
			return (<div key={index} className="spectrometer-bar" style={style}></div>)
		})
		setBars(spectrometer)

	}, [data])

	return (
		<div className="spectrometer-container">{bars}</div >)
}

export default Spectrometer
