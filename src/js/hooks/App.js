import React, { useContext, useState, useEffect } from 'react';
import { StoreContext } from '../context/store';
import { types } from '../context/reducers';

import Workstation from './Workstation'

const App = () => {
	return (
		<div>
			<Workstation />
		</div>
	)
}

export default App