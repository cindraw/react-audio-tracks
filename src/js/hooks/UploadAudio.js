/*
*	This hook receives a file from state.audio_files_for_upload and puts it in the server.
*	It logs the response.data into state.audio_files_to_download reducer so that it is ready to be called by the app.
*/


import React, { useContext, useState, useEffect } from 'react'
import { StoreContext } from '../context/store'
import { types } from '../context/reducers'
import putFile from '../modules/putFile'

const UploadAudio = () => {
	const { state, dispatch, actions } = useContext(StoreContext)
	const audiofile = state.audio_files_for_upload[state.audio_files_for_upload.length - 1]

	useEffect(() => {
		if (audiofile) uploadAudioFile()
	}, [audiofile])

	const uploadAudioFile = async () => {
		const data = await putFile(state.server.root, audiofile)
		actions.logAudioFileToDownloadQueu(data)
		actions.logMessage(data.originalname + ' uploaded')
	}

	return null

}

export default UploadAudio