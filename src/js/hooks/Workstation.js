/*
This is the main kahuna, the big honcho, this is where the audio actually happens
I could have settled for using html <audio> and the exercise would have been done,
but the web audio API has too many awesome possiblities that one cannot resist getting wet.
*/

import React, { useContext, useState, useEffect } from 'react'
import { StoreContext } from '../context/store'
import { types } from '../context/reducers'
import getFile from '../modules/getFile'
import Track from './Track'
import LoadTrack from './LoadTrack'
import Modal from './Modal'
import UploadAudio from './UploadAudio'

let playAnimation // animationframereate

const Workstation = () => {
	// AUDIO CONTEXT
	const AudioContext = window.AudioContext || window.webkitAudioContext
	const audioContext = new AudioContext()

	// STATE, DISPATCH, ACTIONS, CONST
	const { state, dispatch, actions } = useContext(StoreContext)
	const [tracks, setTracks] = useState([])
	const [links, setLinks] = useState([])
	const [audioObjects, setAudioObjects] = useState([])
	const [activatePlaypause, setActivatePlaypause] = useState(false)
	const [frequency, setFrequency] = useState({})
	const [isHidden, setIsHidden] = useState(false)

	/* MAX AMOUNT OF TRACKS as is allowed by preferences determined in state */
	const max = state.maximum_tracks_number
	const files = state.audio_files_to_download
	const amount = files.length > max ? max : files.length

	// HIDE UPLOAD BUTTON when 'max' tracks are loaded
	useEffect(() => {
		state.audio_files_for_upload.length >= max ?
		setIsHidden(false) :
		setIsHidden(true)
	}, [state.audio_files_for_upload])

	// CREATE TRACKS get 'max' amount audio ojects from state and make Track instances
	useEffect(() => {
		// button activate deactivate
		if (files.length === 0) setActivatePlaypause(true)
		else setActivatePlaypause(false)
		// slice 2 last entries of files
		const filesSliced = files.slice(files.length - amount, files.length)
		const tracks = filesSliced.map((q, i) => (<Track key={i} data={q} index={i} />))
		setTracks(tracks)
	}, [files])

	// EXTRACT LINKS
	useEffect(() => {
		const links = files.map(q => q.path)
		setLinks(links)
	}, [files])

	// DOWNLOAD AUDIO AND PREP AUDIOCONTEXT : when links are extracted, creat audioObjects audio.gain.destination
	/*
	*	HERE THERE IS THE PROBLEM OF SAFARI BUGGIN AT createMediaElementSource
	*	possible that the file hasn't loaded fully
	*	possibly need to transform this to audiobuffer or stream or donwload async with fetch
	*/
	useEffect(() => {
		const obj = links.map((el, i) => {
			const id = i
			// create new html audio
			const audio = new Audio(el)
			// create audio source
			const track = audioContext.createMediaElementSource(audio)
			// connect analyser
			const analyser = audioContext.createAnalyser()
			// Uint8Array: Is an unsigned long value half that of the FFT size.
			// This generally equates to the number of data values you will have to play with for the visualization.
			const frequencyData = new Uint8Array(analyser.frequencyBinCount)
			// connect gain
			const gainNode = audioContext.createGain()
			// we have to connect the MediaElementSource with the analyser
			track.connect(analyser)
			track.connect(gainNode).connect(audioContext.destination)
			return { id, audio, gainNode, analyser, frequencyData }
		})
		setAudioObjects(obj)
	}, [links])

	// VOLUME : get volumes from state and set volumes to audio tracks in audioObjects
	useEffect(() => {
		const { id, num } = state.track_volumes
		audioObjects[id] ? audioObjects[id].gainNode.gain.value = num : null
	}, [state.track_volumes])

	// CONTROLER : audio tracks controler, play/pause, extract and log analyser
	const handleClick = (e) => {
		//handle play pause button
		const innertext = e.target.innerText
		e.target.innerText = innertext === 'play' ? 'pause' : 'play'
		// animation frame rate for audio frequency
		audioObjects.map(el => {
			if (el) {
				const renderFrame = () => {
					if (playAnimation) {
						window.requestAnimationFrame(renderFrame)
						el.analyser.getByteFrequencyData(el.frequencyData)
						setFrequency({ id: el.id, frequency: el.frequencyData })
					} else { return }
				}
				// check if context is in suspended state (autoplay policy)
				if (audioContext.state === 'suspended') {
					audioContext.resume()
				}
				//play or pause each track
				if (innertext === 'play') {
					el.audio.play()
					playAnimation = true
					renderFrame()
				} else if (innertext === 'pause') {
					el.audio.pause()
					window.cancelAnimationFrame(renderFrame)
					playAnimation = false
				}
			}
		})

	}

	// FREQUENCY : send frequency to spectrometer
	useEffect(() => {
		/* send frequency data to state to be extracted in Tracks>Spectrometer */
		actions.setFrequencyData(frequency)
	}, [frequency])


	// TEMPLATE
	return (
		<div className="no-select">
			<div className="container menu-main">
				<UploadAudio />
				{!isHidden || <LoadTrack />}
				<button
					id="playpause"
					className="custom-button"
					disabled={activatePlaypause}
					onClick={handleClick}>
					play
				</button>
				<Modal />
			</div>
			<div className="workstation container">
				{tracks}
			</div>
		</div>)
}


export default Workstation
