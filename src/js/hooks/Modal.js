/* Modal logs messages for the user to see */

import React, { useContext, useState, useEffect } from 'react'
import { StoreContext } from '../context/store'
import { types } from '../context/reducers'

const Modal = () => {
	const { state, dispatch, actions } = useContext(StoreContext)
	const lastmessage = state.message[state.message.length-1]
	return (
		<div className="modal no-select">
			{lastmessage}
		</div>
	)

}

export default Modal