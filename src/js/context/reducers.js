/* reducer */

const { PORT } = process.env

const initialState = {
	show_state_in_console: false,
	message: [''],
	audio_files_for_upload: [],
	audio_files_to_download: [],
	server: {
		root: `http://localhost:${PORT}/`,
		uploads: `http://localhost:${PORT}/uploads`
	},
	maximum_tracks_number: 2,
	track_volumes: {},
	frequency_data: {},
	new_track_volumes: {}
}

const types = {
	LOG_MESSAGE: 'LOG_MESSAGE',
	LOG_AUDIO_FILE_TO_UPLOAD: 'LOG_AUDIO_FILE_TO_UPLOAD',
	LOG_AUDIO_FILE_TO_DOWNLOAD: 'LOG_AUDIO_FILE_TO_DOWNLOAD',
	TRACK_VOLUMES: 'TRACK_VOLUMES',
	NEW_TRACK_VOLUMES: 'NEW_TRACK_VOLUMES',
	FREQUENCY_DATA: 'FREQUENCY_DATA',
}

const reducer = (state = initialState, action) => {
	if (state.show_state_in_console) console.log({ oldState: state, type: action.type, payload: action.payload })

	switch (action.type) {

		case types.LOG_MESSAGE:
			return {
				...state,
				message: [...state.message, action.payload]
			}

		case types.LOG_AUDIO_FILE_TO_UPLOAD:
			return {
				...state,
				audio_files_for_upload: [...state.audio_files_for_upload, action.payload]
			}

		case types.LOG_AUDIO_FILE_TO_DOWNLOAD:
			return {
				...state,
				audio_files_to_download: [...state.audio_files_to_download, action.payload]
			}

		case types.TRACK_VOLUMES:
			return {
				...state,
				track_volumes: { id: action.payload.id, num: action.payload.num }
			}
		case types.NEW_TRACK_VOLUMES:
			return {
				...state,
				new_track_volumes: { [action.payload.id]: action.payload.volume }
			}

		case types.FREQUENCY_DATA:
			return {
				...state,
				frequency_data: { [action.payload.id]: action.payload.frequency }
			}

		default:
			throw new Error('Unexpected Action')

	}
}

export { initialState, types, reducer }