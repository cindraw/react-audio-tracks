/* context store */

import React, { createContext, useReducer, useEffect } from 'react'
import { reducer, initialState } from './reducers'
import { useActions } from './actions'

const StoreContext = createContext(initialState)

const StoreProvider = ({ children }) => {

	const [state, dispatch] = useReducer(reducer, initialState)
	const actions = useActions(state, dispatch)

	useEffect(() => {
		/* this effect logs the state into the console. It is activated and deactivated in the reducer at state.show_state_in_console */
		if (state.show_state_in_console) console.log({ newState: state })
	}, [state])


	return (
		// context
		<StoreContext.Provider value={{ state, dispatch, actions }}>
			{children}
		</StoreContext.Provider>
	)
}

export { StoreContext, StoreProvider }
