/* context actions */

import { StoreContext } from "./store"
import { types } from './reducers'

export const useActions = (state, dispatch) => {
	return {

		// modal
		getLastMessage() {
			return state.message[state.message.length - 1]
		},
		logMessage(message) {
			dispatch({ type: types.LOG_MESSAGE, payload: message })
			setTimeout(() => this.logEmptyMessage(), 3000)
		},
		logEmptyMessage() {
			dispatch({ type: types.LOG_MESSAGE, payload: '' })
		},

		// used to filter filetypes in uploads
		checkMimeTypeIsAudio(file) {
			return file.type.includes('audio')
		},

		// infos to be exchanged between uploads and workstation
		logAudioFileToUploadQueu(file) {
			dispatch({ type: types.LOG_AUDIO_FILE_TO_UPLOAD, payload: file })
		},
		logAudioFileToDownloadQueu(file) {
			dispatch({ type: types.LOG_AUDIO_FILE_TO_DOWNLOAD, payload: file })
		},

		// send infos to be exchanged between workstation and tracks
		setTrackVolumes({ id, num }) {
			dispatch({ type: types.TRACK_VOLUMES, payload: { id: id, num: num } })
			dispatch({ type: types.NEW_TRACK_VOLUMES, payload: { id: id, volume: num } })
		},
		setFrequencyData({ id, frequency }) {
			dispatch({ type: types.FREQUENCY_DATA, payload: { id: id, frequency: frequency } })
		},

	}
}