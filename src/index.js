require('dotenv').config()

import React from 'react'
import ReactDOM from 'react-dom'
import App from './js/hooks/App'
import './css/index.css'
import { StoreProvider } from './js/context/store'

ReactDOM.render(
	<StoreProvider>
		<App />
	</StoreProvider>,
	document.querySelector("#root"))
